from .dto import ChoiceDTO, QuestionDTO, QuizDTO, AnswerDTO, AnswersDTO
from typing import List


class QuizResultService():
    def __init__(self, quiz_dto: QuizDTO, answers_dto: AnswersDTO):
        self.quiz_dto = quiz_dto
        self.answers_dto = answers_dto
    def get_result(self):# -> float:
        result = 0

        for a in self.answers_dto.answers:
            buffer = self.quiz_dto.questions[int(a.question_uuid)-1].choices
            count = 0
            all_virgin = True
            for b in buffer:
                if b.is_correct == True:
                    virgin = True
                    for c in a.choices:
                        if int(c) == int(b.uuid):
                            virgin = False
                            count += 1
                    if virgin == True:
                        all_virgin=False
                        break
            if all_virgin == True and count == len(a.choices):
                result += (100/len(self.answers_dto.answers))
        return round(result,2)


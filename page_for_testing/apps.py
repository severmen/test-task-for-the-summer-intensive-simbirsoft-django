from django.apps import AppConfig


class PageForTestingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'page_for_testing'

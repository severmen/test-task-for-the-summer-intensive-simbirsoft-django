import os
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
from quiz.services import QuizResultService
from quiz.dto import ChoiceDTO, QuestionDTO, QuizDTO, AnswerDTO, AnswersDTO
from datetime import datetime

instance_QuestionDTO = None
instance_AnswerDTO = []
'''
    Функция index
    Входные данные:только request
    Выходные данные: не имеет
    Функция возвращает страницу при обращении к '/'
'''
def index(request):
    current_datetime = datetime.now()
    return render(request, 'page_for_testing/start.html',{'time_hours': current_datetime.hour, 'minutes': current_datetime.minute})

'''
    Функция read_and_initializations_QuestionDTO
    Входные данные:не имеет
    Выходные данные: не имеет
    Функция считывает вопросы из файла '/page_for_testing/Test_for_the_app'
    записывает вопросы в instance_QuestionDTO(объявлен глобально) которые имеет тип QuestionDTO
    также инициализирует instance_AnswerDTO(объявлен глобально), но сами ответы пользователи пустые
'''
def read_and_initializations_QuestionDTO():
    global instance_QuestionDTO
    global instance_AnswerDTO
    instance_AnswerDTO = []
    instance_QuestionDTO = []

    module_dir = os.path.dirname(__file__)
    file_path = os.path.join(module_dir, 'Test_fort_the_app')  # full path to text.
    file = open(file_path, 'r', encoding='utf-8')
    count_answer = int(file.readline())
    for a in range(count_answer):
        buffer = file.readline().split("\n")
        if buffer[0] != ".":
            break
        buffer_count = 0
        ChDTO = []

        for b in range(1, 10):
            buffer_count += 1
            buffer = file.readline()
            try:
                buffer2 = buffer.split("\n")
                if buffer[0] == ":":
                    break
            except Exception:
                pass
            buffer = buffer.split(";")
            buffer_uuid = buffer[0]
            buffer_text = buffer[1]
            if buffer[2] == "True":
                buffer_is_correct = True
            if buffer[2] == "False":
                buffer_is_correct = False
            ChDTO.append(ChoiceDTO(buffer_uuid, buffer_text, buffer_is_correct))

        if buffer_count < 2:
            break
        # Записываем сам вопрос

        buffer = file.readline().split(";")
        if len(buffer) == 1:
            buffer = file.readline().split(";")
        instance_QuestionDTO.append(QuestionDTO(buffer[0], buffer[1], ChDTO))
        instance_AnswerDTO.append(AnswerDTO(buffer[0], []))
'''
    Функция sortAnswerDTO
    Входные данные: не имеет
    Выходные данные: не имеет
    Функция сортирует instance_AnswerDTO(объявлён глобально) по uuid в
    в поредке возрастания
'''
def sortAnswerDTO():
    global instance_AnswerDTO
    for a in range(len(instance_AnswerDTO)):
        for b in range(len(instance_AnswerDTO) - 1):
            buffer1 = instance_AnswerDTO[b]
            buffer2 = instance_AnswerDTO[b + 1]
            if int(buffer1.question_uuid) > int(buffer2.question_uuid):
                buffer1 = instance_AnswerDTO[b]
                instance_AnswerDTO[b] = instance_AnswerDTO[b + 1]
                instance_AnswerDTO[b + 1] = buffer1
                break

'''
    Функция question
    Входные данные: request,way(кем была вызвана страница 'F'-кнопкой пройти тест
    'N'-копкой следующий вопрос
    '0'-кнопкой предыдущий вопрос
    'L'-кнопкой завершить тест
    ), number(номер вопроса, целочисленное число)
    Выходные данные: сгенерированную страницу или ошибку 404
'''

@csrf_exempt
def question(request,way, number):
    global instance_QuestionDTO
    global instance_AnswerDTO
    '''
    Считываем данные
    инициализируем класс
    отправлем в фронт
    '''
    if instance_QuestionDTO == None or way == "F":
        read_and_initializations_QuestionDTO()
    if way == "N" or way == "L":
        user_answer = []
        for a in request.GET:
            user_answer.append(str(a))
        for a in range(len(instance_AnswerDTO)):
            if int(instance_AnswerDTO[a].question_uuid) == int(number):
                del instance_AnswerDTO[a]
                break

        instance_AnswerDTO.append(AnswerDTO(str(int(number)), user_answer))
        #сортируем по uuid
        sortAnswerDTO()

    if way == "L":
        return redirect("/result")
    try:
        if int(number) == 0:
            return render(request, 'page_for_testing/page_for_questions.html', {'number_question':str(int(number)+1), 'data' :instance_QuestionDTO[int(number)],
                                                             "next_url":"requestsN"+str(int(number)+1),"text_next_url":"Следующий вопрос",
                                                                            "old_url":"", "AnswerDTO":instance_AnswerDTO[int(number)] })
        elif int(number) == len(instance_QuestionDTO)-1:
            return render(request, 'page_for_testing/page_for_questions.html',
                          {'number_question': str(int(number) + 1), 'data': instance_QuestionDTO[int(number)],
                           "next_url": "requestsL" + str(int(number) + 1), "text_next_url": "Закончить тест",
                           "old_url": "requestsO" + str(int(number) - 1), "AnswerDTO":instance_AnswerDTO[int(number)]})
        else:
            return render(request, 'page_for_testing/page_for_questions.html',
                          {'number_question': str(int(number) + 1), 'data': instance_QuestionDTO[int(number)],
                           "next_url": "requestsN" + str(int(number) + 1), "text_next_url": "Следующий вопрос",
                           "old_url": "requestsO" + str(int(number) - 1), "AnswerDTO":instance_AnswerDTO[int(number)]})
    except Exception:
        return HttpResponse(status = 404)

'''
    Функция result
    Входные данные: request
    Выходные данные: сгенерированную страницу c результатом
    функция создаёт экзмеляр класса QuizResultService и вызвв его метод get_result()
    определят результаты теста и оправлет в файл  result.html
'''

def result(requset):
    global instance_QuestionDTO
    global instance_AnswerDTO
    instances_QuizResultService = QuizResultService(QuizDTO("1", "Тест по Python django", instance_QuestionDTO),
                          AnswersDTO("1",instance_AnswerDTO))
    #что то не работает переобялвем заного
    instance_QuestionDTO = None
    instance_AnswerDTO = []
    return render(requset, 'page_for_testing/result.html',
                  {'result':str(instances_QuizResultService.get_result())})
